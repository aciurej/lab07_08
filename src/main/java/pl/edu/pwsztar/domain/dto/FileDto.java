package pl.edu.pwsztar.domain.dto;

import java.util.List;

public class FileDto {
    private List<MovieDto> fileDtoList;

    public FileDto(List<MovieDto> fileDtoList) {
        this.fileDtoList = fileDtoList;
    }

    public List<MovieDto> getFileDtoList() {
        return fileDtoList;
    }

}
