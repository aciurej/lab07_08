package pl.edu.pwsztar.domain.files;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.service.MovieService;

import java.io.*;
import java.util.List;

@Service
public class FileServiceImpl implements FileGenerator{
    private MovieService movieService;

    @Autowired
    public FileServiceImpl(MovieService movieService) {
        this.movieService = movieService;
    }
    @Override
    public InputStreamResource toTxt(FileDto fileDto) throws IOException {

        File f = File.createTempFile("tmp", ".txt");
        FileOutputStream fos = new FileOutputStream(f);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        List<MovieDto> moviesSorted = movieService.sortMovies();
        for (MovieDto movie: moviesSorted) {
            bw.write(movie.getYear() + " " + movie.getTitle());
            bw.newLine();
        }
        bw.close();

        fos.flush();
        fos.close();

        InputStream stream = new FileInputStream(f);

        return new InputStreamResource(stream);
    }
}
